import re
import sys
import time
import numpy as np

import NLP
import constants
import utils


def parseTrainTestFile(filePath):
    """
    reads train or test file and inits corpus objects

    :param filePath: train or test file path
    :return: initialized Corpus object
    """
    file = open(filePath, "r")
    data = file.read()
    corpus_tag = re.findall(constants.CORPUS_REGEX, data)[0]
    return NLP.Corpus(corpus_tag)


def evaluateTest(train_corpus, test_corpus, outFilePath):
    """
    evaluates test corpus with train corpus.
    writes optimum sense ids to @outputFilePath

    :param train_corpus: trained corpus object
    :param test_corpus: test corpus object
    :param outFilePath: output file absolute path
    :return: None
    """
    outFile = open(outFilePath, "w")
    for test_lexelt in test_corpus.getLexeltList():
        # get lexelt list from test corpus
        train_sense_id_list = train_corpus.getSenseListEquals(test_lexelt.getItem())
        # calculate all of header neighbor count according to sense id list
        all_sense_count = NLP.getAllOfSenseCount(train_sense_id_list, test_lexelt.getSenseHeaderDict())
        for test_instance in test_lexelt.getInstanceList():
            # calculate test instances probs one by one
            sense_prob_list = []
            for sense_id in train_sense_id_list:
                train_sense_headers = test_lexelt.getSenseHeaderDict()[sense_id]
                len_train_sense_headers = len(train_sense_headers)
                probability_of_one_sense_id = 1
                for test_headerNeigbor in test_instance.getHeaderList():
                    # calculate items probability
                    item_count = NLP.neighborItemCounter(test_headerNeigbor, train_sense_headers)
                    if item_count == 0:
                        probability_of_one_sense_id *= (1 / (all_sense_count * 2))
                    else:
                        probability_of_one_sense_id *= (item_count / (len_train_sense_headers * 2))

                    # calculate types probability
                    type_index_count = NLP.neighborTypeIndexCounter(test_headerNeigbor, train_sense_headers)
                    if type_index_count == 0:
                        probability_of_one_sense_id *= (1 / (all_sense_count * 2))
                    else:
                        probability_of_one_sense_id *= (type_index_count / (len_train_sense_headers * 2))
                # calculate prior
                probability_of_one_sense_id *= (len_train_sense_headers / all_sense_count)
                sense_prob_list.append(probability_of_one_sense_id)
            max_index = np.argmax(sense_prob_list)
            # get optimum sense
            optimum_sense = train_sense_id_list[max_index]
            utils.writeLineToFile(outFile, test_instance.getID() + " " + optimum_sense)
            # print(test_instance.getID(), optimum_sense)
        utils.writeLog("LEXELT-ANALYSIS", test_lexelt.getItem() + " is completed.")
    utils.writeLog("EVALUATION", "Evaluation of test corpus is completed. Please see: " + outFilePath)
    outFile.close()
    #############################################################################################################
    # i know code looks like too much complicated by for loops. (many for loops :))                             #
    # but in my project everything is object and objects contains subtag lists                                  #
    # corpus contains lexelt lists, lexelt contains instance list, instance contains header neighbor list...    #
    # so my code is little bit slower than my friend's codes.                                                   #
    # ###########################################################################################################


def main(argv):
    train_corpus = parseTrainTestFile(argv[1])
    utils.writeLog("TRAIN-PROCESS", "Train Corpus is parsed successfully. Language: " + train_corpus.getLanguage())
    test_corpus = parseTrainTestFile(argv[2])
    utils.writeLog("TEST-PROCESS", "Test Corpus is parsed successfully.")
    evaluateTest(train_corpus, test_corpus, argv[3])


if __name__ == "__main__":
    utils.writeLog("MAIN-PROCESS", "Word Sense Disambiguation program is started.")
    start = time.time()
    main(sys.argv)
    utils.writeLog("MAIN-PROCESS",
                   "Word Sense Disambiguation program is finished in " + str(time.time() - start) + " seconds.")
