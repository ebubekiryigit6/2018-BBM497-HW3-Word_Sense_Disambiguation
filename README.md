#  Word Sense Disambiguation using Naive Bayes  

BBM497 - Introduction to NLP Lab.

Assignment 3

### Author
Ebubekir Yiğit - 21328629



### Project Introduction
First we parse the tags like
```text
<corpus lang="en">
<lexelt item="accident-n">
<instance id="accident-n.800001">
<answer instance="accident-n.800001" senseid="532675"/>
<context>
```
in the train and test file and initialize all the objects. 
We then calculate the optimal sense id for all instances in the test data. 
After the calculations are done, the instance id is printed to the optimal sense id output file.
## Getting Started

In the project, I chose to keep many arrays, 
objects that consist of tags instead of holding a dictionary. 
The reason for this is that it is easier to parse and debugging is easier. 
But I had to calculate the same thing more than once in the test part and the code worked more slowly.


In the project I used the Corpus, Lexelt, 
Instance and HeaderNeighbor classes in the NLP package. 
These classes are initialized with strings containing <corpus> </corpus> tags that come with regex.

When the test account is run in the project, 
the probabilities are calculated for every possible sense id and 
the maximum probability is found using the argmax() function 
in the numpy library and the optimum sense is written.

**Note:** When the probability is calculated, 
we use 3 neighbors from the left and 3 neighbors 
from the right when we train, not all header neighbors.


**All operations are progressively saved to the console.log file. 
You can browse the console.log file to see the operations.**


**Corpus:**
```python
    import NLP
    import constants
    import re
    
    corpus_tag = re.findall(constants.CORPUS_REGEX, data)[0]
    corpus_obj = NLP.Corpus(corpus_tag)
    corpus_obj.getLanguage()                                        # returns corpus language
    corpus_obj.getLexeltList()                                      # returns lexelt list
```


**Lexelt:**
```python
    import NLP
    import re
    import constants
    
    lexelt_tag = re.findall(constants.LEXELT_REGEX, data)[0]
    lexelt_obj = NLP.Lexelt(lexelt_tag)
    lexelt_obj.getItem()                                            # returns lexelt item
    lexelt_obj.getInstanceList()                                    # returns instance list
```   

**Instance:**
```python
    import NLP
    import re
    import constants
    
    instance_tag = re.findall(constants.INSTANCE_REGEX, data)[0]
    instance_obj = NLP.Instance(instance_tag)
    instance_obj.getID()                                            # returns instance id
    instance_obj.getSenseID()                                       # returns sense id
    instance_obj.getContext()                                       # returns context tag
    instance_obj.getHeaderList()                                    # returns header list
```   

**Stopword file path is statically defined in constants.py file. 
You can simply change stopword file path. (Default file path is: "./stopwords.txt")**


### Output
```text
accident-n.700001 532675
accident-n.700002 532675
accident-n.700003 532675
accident-n.700004 532675
accident-n.700005 532675
accident-n.700006 532675
accident-n.700007 532675
accident-n.700008 538889
accident-n.700009 532675
accident-n.700010 538889
accident-n.700011 538889
accident-n.700012 532675
accident-n.700013 532675
accident-n.700014 532675
accident-n.700015 532675
accident-n.700016 532674
accident-n.700017 532675
accident-n.700018 532675
accident-n.700019 532675
accident-n.700020 532675
```




### Prerequisites

Python 3.5 is required to run the project. 

**Used Python libraries:**

- numpy


For Ubuntu:

```bash
sudo apt-get install python3.5
```

For Centos:
```bash
sudo yum -y install python35u
```

### Running the Project

python3 assignment3.py <train-corpus> <test-corpus> <output_file>

Example:
```text
python3 assignment3.py train-S1.pos test-S1.pos out.txt
```

**After run, please see "console.log" file to show process logs.**

### console.log File
```text
21:53:14   LOG/ MAIN-PROCESS:  Word Sense Disambiguation program is started.
21:53:22   LOG/ TRAIN-PROCESS:  Train Corpus is parsed successfully. Language: en
21:53:26   LOG/ TEST-PROCESS:  Test Corpus is parsed successfully.
21:54:02   LOG/ LEXELT-ANALYSIS:  accident-n is completed.
21:54:02   LOG/ LEXELT-ANALYSIS:  amaze-v is completed.
21:54:04   LOG/ LEXELT-ANALYSIS:  band-p is completed.
21:54:05   LOG/ LEXELT-ANALYSIS:  behaviour-n is completed.
21:54:05   LOG/ LEXELT-ANALYSIS:  bet-n is completed.
21:54:05   LOG/ LEXELT-ANALYSIS:  bet-v is completed.
21:54:06   LOG/ LEXELT-ANALYSIS:  bitter-p is completed.
21:54:06   LOG/ LEXELT-ANALYSIS:  bother-v is completed.
21:54:06   LOG/ LEXELT-ANALYSIS:  brilliant-a is completed.
21:54:07   LOG/ LEXELT-ANALYSIS:  bury-v is completed.
21:54:07   LOG/ LEXELT-ANALYSIS:  calculate-v is completed.
21:54:07   LOG/ LEXELT-ANALYSIS:  consume-v is completed.
21:54:07   LOG/ LEXELT-ANALYSIS:  derive-v is completed.
21:54:07   LOG/ LEXELT-ANALYSIS:  float-n is completed.
21:54:07   LOG/ LEXELT-ANALYSIS:  float-v is completed.
21:54:07   LOG/ LEXELT-ANALYSIS:  floating-a is completed.
21:54:08   LOG/ LEXELT-ANALYSIS:  generous-a is completed.
21:54:08   LOG/ LEXELT-ANALYSIS:  giant-a is completed.
21:54:08   LOG/ LEXELT-ANALYSIS:  giant-n is completed.
21:54:08   LOG/ LEXELT-ANALYSIS:  invade-v is completed.
21:54:09   LOG/ LEXELT-ANALYSIS:  knee-n is completed.
21:54:09   LOG/ LEXELT-ANALYSIS:  modest-a is completed.
21:54:09   LOG/ LEXELT-ANALYSIS:  onion-n is completed.
21:54:09   LOG/ LEXELT-ANALYSIS:  promise-n is completed.
21:54:10   LOG/ LEXELT-ANALYSIS:  promise-v is completed.
21:54:10   LOG/ LEXELT-ANALYSIS:  sack-n is completed.
21:54:10   LOG/ LEXELT-ANALYSIS:  sack-v is completed.
21:54:11   LOG/ LEXELT-ANALYSIS:  sanction-p is completed.
21:54:11   LOG/ LEXELT-ANALYSIS:  scrap-n is completed.
21:54:11   LOG/ LEXELT-ANALYSIS:  scrap-v is completed.
21:54:11   LOG/ LEXELT-ANALYSIS:  seize-v is completed.
21:54:12   LOG/ LEXELT-ANALYSIS:  shake-p is completed.
21:54:13   LOG/ LEXELT-ANALYSIS:  shirt-n is completed.
21:54:14   LOG/ LEXELT-ANALYSIS:  slight-a is completed.
21:54:14   LOG/ EVALUATION:  Evaluation of test corpus is completed. Please see: out.txt
21:54:14   LOG/ MAIN-PROCESS:  Word Sense Disambiguation program is finished in 59.26544642448425 seconds.
```
