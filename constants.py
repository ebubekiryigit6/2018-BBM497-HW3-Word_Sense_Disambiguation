CORPUS_REGEX = r'<corpus[\S\s]*?<\/corpus>'
CORPUS_LANGUAGE_REGEX = r'(?<=lang=).+?(?=>)'
LEXELT_REGEX = r'<lexelt[\S\s]*?<\/lexelt>'
LEXELT_ITEM_REGEX = r'(?<=item=).+?(?=>)'
INSTANCE_REGEX = r'<instance[\S\s]*?<\/instance>'
INSTANCE_ID_REGEX = r'<instance.*?>'
SENSE_ID_REGEX = r'(?<=senseid=).+?(?=>)'
CONTEXT_REGEX = r'<context>([\s\S]*?)</context>'
CONTEXT_HEAD_REGEX = r'<head>[\s\S]*?<\/head>'
CONTEXT_NEIGHBOR_REGEX_WITH_DELIMITER = '(<p[\s\S]*?/>)'
CONTEXT_NEIGHBOR_REGEX = r'[\s\S]*?<p=[\s\S]*?\/>'

STOPWORDS_FILE_PATH = "./stopwords.txt"
LOG_FILE = "console.log"
