import re

import constants
import stemmer

TR_LANG = "tr"
EN_LANG = "en"
GR_LANG = "gr"

NONE_TYPE = "NONE"


def getStopWords():
    """
    read statically stopword.txt file and returns it as array
    :return: array of stopwords
    """
    stopwords = []
    with open(constants.STOPWORDS_FILE_PATH, "r") as file:
        for line in file:
            line = line.strip("\n").strip()
            if line:
                stopwords.append(line)
    return stopwords


def getAllOfSenseCount(sense_list, sense_header_dict):
    """
    gets all of "accident-n" unique sense ids and counts it.

    :param sense_list: sense id list
    :param sense_header_dict: all of sense dict
    :return: returns count of sense id values of dict
    """
    sum = 0
    for i in sense_list:
        sum += len(sense_header_dict[i])
    return sum


def neighborItemCounter(query, n_list):
    count = 0
    for i in n_list:
        if query.getItem() == i.getItem():
            count += 1
    return count


def neighborTypeIndexCounter(query, n_list):
    count = 0
    for i in n_list:
        if query.getType() == i.getType() and query.getIndex() == i.getIndex():
            count += 1
    return count


class Corpus:
    __lang = ""
    __lexelt_list = []

    def __init__(self, corpus_tag):
        self.__lang = EN_LANG
        self.__lexelt_list = []
        self.__initCorpus(corpus_tag)

    def getLanguage(self):
        return self.__lang

    def getLexeltList(self):
        return self.__lexelt_list

    def getSenseListEquals(self, lexelt_id):
        temp = []
        for lexelt in self.__lexelt_list:
            if lexelt.getItem() == lexelt_id:
                for instance in lexelt.getInstanceList():
                    if instance.getSenseID() not in temp:
                        temp.append(instance.getSenseID())
                break
        return temp

    def __initCorpus(self, corpus_tag):
        # find corpus tags from file
        self.__lang = re.findall(constants.CORPUS_LANGUAGE_REGEX, corpus_tag)[0].strip("\"").strip()
        # init and append lexelts
        lexelts = re.findall(constants.LEXELT_REGEX, corpus_tag)
        for lexelt in lexelts:
            self.__lexelt_list.append(Lexelt(lexelt))


class Lexelt:
    __item = ""
    __instance_list = []
    __sense_header_dict = {}

    def __init__(self, lexelt_tag):
        self.__item = NONE_TYPE
        self.__instance_list = []
        self.__initLexelt(lexelt_tag)

    def getItem(self):
        return self.__item

    def getInstanceList(self):
        return self.__instance_list

    def getSenseHeaderDict(self):
        return self.__sense_header_dict

    def __initLexelt(self, lexelt_tag):
        # find lexelt item
        self.__item = re.findall(constants.LEXELT_ITEM_REGEX, lexelt_tag)[0].strip("\"").strip()
        # detect all instances
        instances = re.findall(constants.INSTANCE_REGEX, lexelt_tag)
        for instance in instances:
            # init and append instances
            ins = Instance(instance)
            if ins.getSenseID() in self.__sense_header_dict:
                self.__sense_header_dict[ins.getSenseID()].extend(ins.getHeaderList())
            else:
                self.__sense_header_dict[ins.getSenseID()] = ins.getHeaderList()
            self.__instance_list.append(ins)


class Instance:
    __id = ""
    __sense_id = ""
    __context = ""
    __header_list = []

    def __init__(self, instance_tag):
        self.__id = ""
        self.__sense_id = ""
        self.__context = ""
        self.__header_list = []
        self.__initInstance(instance_tag)

    def getID(self):
        return self.__id

    def getSenseID(self):
        return self.__sense_id

    def getContext(self):
        return self.__context

    def getHeaderList(self):
        return self.__header_list

    def __initInstance(self, instance_tag):
        # get instance id and sense id from instance tags
        self.__id = re.findall(constants.INSTANCE_ID_REGEX, instance_tag)[0].split("\"")[1]
        sense_id = re.findall(constants.SENSE_ID_REGEX, instance_tag)
        if len(sense_id) > 0:
            # test data crashes because there is no sense id in test
            self.__sense_id = sense_id[0].strip("/").strip("\"").strip()
        self.__context = re.findall(constants.CONTEXT_REGEX, instance_tag)[0].strip("\n").strip()
        # get left and right of <head> tag
        left_right_neighbors = re.split(constants.CONTEXT_HEAD_REGEX, self.__context)
        left = left_right_neighbors[0]
        right = left_right_neighbors[1]
        left = re.findall(constants.CONTEXT_NEIGHBOR_REGEX, left)
        right = re.findall(constants.CONTEXT_NEIGHBOR_REGEX, right)

        # get 3 of left
        for i in range(len(right)):
            if i == 3:
                break
            item = re.findall(r'.+?(?=<)', right[i])[0].strip()
            type = re.findall(r'(?<=p=).+?(?=/>)', right[i])[0].strip("\"").strip()
            index = i + 1
            self.__header_list.append(HeaderNeighbor(item, type, index))

        # get 3 of right
        count = 0
        for j in reversed(left):
            if count == 3:
                break
            item = re.findall(r'.+?(?=<)', j)[0].strip()
            type = re.findall(r'(?<=p=).+?(?=/>)', j)[0].strip("\"").strip()
            index = -(count + 1)
            self.__header_list.append(HeaderNeighbor(item, type, index))
            count += 1

        # delete stopword objects
        self.__handleStopWords()
        # change items with stemmer outputs
        self.__changeWordsWithStemmer()

    def __handleStopWords(self):
        stopwords = getStopWords()
        temp = []
        for header in self.__header_list:
            if header.getItem() not in stopwords:
                temp.append(header)
        self.__header_list = temp

    def __changeWordsWithStemmer(self):
        for header in self.__header_list:
            item = header.getItem()
            stem = stemmer.PorterStemmer().stem(item, 0, len(item) - 1)
            header.setItem(stem)


class HeaderNeighbor:
    __item = ""
    __TYPE = ""
    __index = 0

    def __init__(self, item, typer, index):
        self.__item = item
        self.__TYPE = typer
        self.__index = index

    def setItem(self, item):
        self.__item = item

    def getItem(self):
        return self.__item

    def getType(self):
        return self.__TYPE

    def getIndex(self):
        return self.__index
